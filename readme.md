Einnovart\PHPDocX
=================

> This package can merge Microsoft DocX files and it can replace the variables. The merged files should be the same by
same footer, same header, same style. External library is not required for this package.

* Examples are in tests: Einnovart\PHPDocX\DocxApiTest()
* Variable can be ${test} for example.
* install: 
    * composer update
    * composer dumpautoload
* test: composer test