<?php

namespace Einnovart\PHPDocX;

use PHPUnit_Framework_TestCase;

class DocxApiTest extends PHPUnit_Framework_TestCase
{
    private static $testDir;

    private static $ds = DIRECTORY_SEPARATOR;

    private static $resultMerge;

    private static $resultReplace;

    public static function setUpBeforeClass()
    {
        self::$testDir = realpath(dirname(__FILE__));
        self::$resultMerge = self::$testDir . self::$ds . 'result_merge.docx';
        self::$resultReplace = self::$testDir . self::$ds . 'result_replace.docx';

        if (file_exists(self::$resultMerge)) {
            unlink(self::$resultMerge);
        }

        if (file_exists(self::$resultReplace)) {
            unlink(self::$resultReplace);
        }
    }

    public function testMergeDocuments()
    {
        $api = DocxApi::getInstance();
        $api->addDocumentsPath(array(
            self::$testDir . self::$ds . 'part1.docx',
            self::$testDir . self::$ds . 'part2.docx'
        ));
        $api->addResultPath(self::$resultMerge);
        $result = $api->mergeDocuments();

        $this->assertTrue($result);
        $this->assertTrue(file_exists(self::$resultMerge));
    }

    public function testReplaceVariables()
    {
        $api = DocxApi::getInstance();
        $api->addDocumentsPath(self::$testDir . self::$ds . 'replace.docx');
        $api->addVariableMap(array(
            'test' => 'tralalalala'
        ));
        $api->addResultPath(self::$resultReplace);
        $api->replaceVariables();
        $this->assertTrue(file_exists(self::$resultReplace));
    }
}