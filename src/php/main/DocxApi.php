<?php

namespace Einnovart\PHPDocX;

use Einnovart\PHPDocX\Merge\DocxMerge;
use Einnovart\PHPDocX\Merge\DocxMerge\Merge;
use Einnovart\PHPDocX\Merge\Exception\MergeException;

/**
 * Class DocxApi
 * @package Einnovart\PHPDocX
 * @author David Belicza
 */
final class DocxApi
{
    /**
     * Instance of singleton.
     *
     * @var null
     */
    private static $singleton = null;

    /**
     * @var null
     */
    private $workingDocumentPath = null;

    /**
     * @var null
     */
    private $resultDocumentPath = null;

    /**
     * @var null
     */
    private $variableMap = null;

    /**
     * @return $this
     */
    public static function getInstance()
    {
        if (self::$singleton === null) {
            self::$singleton = new DocxApi();
        }

        return self::$singleton;
    }

    /**
     * @param $documentPaths
     */
    public function addDocumentsPath($documentPaths)
    {
        $this->workingDocumentPath = $documentPaths;
    }

    /**
     * @param $documentPath
     */
    public function addResultPath($documentPath)
    {
        $this->resultDocumentPath = $documentPath;
    }

    /**
     * @param $variableMap
     */
    public function addVariableMap($variableMap)
    {
        $this->variableMap = $variableMap;
    }

    /**
     * @return bool
     * @throws MergeException
     */
    public function mergeDocuments()
    {
        $merge = new Merge();

        return $merge->mergeFiles(
            $this->workingDocumentPath,
            $this->resultDocumentPath
        );
    }

    public function replaceVariables()
    {
        $dm = new DocxMerge();
        $dm->setValues(
            $this->workingDocumentPath,
            $this->resultDocumentPath,
            $this->variableMap
        );
    }
}