<?php

namespace Einnovart\PHPDocX\Merge\Exception;

use Exception;

/**
 * Class MergeException
 *
 * @package Einnovart\PHPDocX\Merge\Exception
 * @author David Belicza
 */
class MergeException extends \Exception
{
    const INVALID_PARAMETERS = 'Merging parameters are non-sense.';
    const INVALID_DOCUMENT = "Invalid document. Tag <w:body> not found.";

    public function __construct($message = "", $code = 0, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}