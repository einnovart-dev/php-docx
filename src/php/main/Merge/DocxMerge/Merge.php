<?php

namespace Einnovart\PHPDocX\Merge\DocxMerge;

use Einnovart\PHPDocX\Merge\Exception\MergeException;
use Einnovart\PHPDocX\Merge\Libraries\TbsZip;

/**
 * Class Merge
 * This class handles docx file merging process.
 *
 * @package Einnovart\PHPDocX\Merge\DocxMerge
 * @author David Belicza
 */
class Merge
{
    /**
     * @var TbsZip
     */
    protected $zip;

    /**
     * Merge constructor.
     */
    public function __construct()
    {
        $this->zip = new TbsZip();
    }

    /**
     * @param null|array $filePathsForMerge It has to be 2 items array at least.
     * @param null|string $resultFilePath It has to be a string for docx path.
     * @return bool
     * @throws MergeException
     */
    public function mergeFiles($filePathsForMerge = null, $resultFilePath = null)
    {
        if (!is_array($filePathsForMerge) || count($filePathsForMerge) <= 1 || !is_string($resultFilePath)) {
            throw new MergeException(MergeException::INVALID_PARAMETERS);
        }

        $firstFilePath = $filePathsForMerge[0];
        unset($filePathsForMerge[0]);

        $content = $this->getFilesContents($filePathsForMerge);
        $this->mergeAllFiles($firstFilePath, $content);

        return $this->saveMergedFile($resultFilePath);
    }

    /**
     * It retrieves all file contents without styles.
     *
     * @param $filePathsForMerge
     * @return string
     * @throws MergeException
     */
    protected function getFilesContents($filePathsForMerge)
    {
        $content = '';

        foreach ($filePathsForMerge as $idx => $filePath) {
            $this->zip->Open($filePath);
            $currentContent = $this->zip->FileRead('word/document.xml');
            $this->zip->Close();

            $p = strpos($currentContent, '<w:body');
            if ($p === false) {
                throw new MergeException(MergeException::INVALID_DOCUMENT);
            }

            $p = strpos($currentContent, '>', $p);
            $currentContent = substr($currentContent, $p+1);
            $p = strpos($currentContent, '</w:body>');
            if ($p === false) {
                throw new MergeException(MergeException::INVALID_DOCUMENT);
            }

            $content .= substr($currentContent, 0, $p);
        }

        return $content;
    }

    /**
     * Insert file contents to the first document.
     *
     * @param $firstFilePath
     * @param $otherFilesContent
     * @throws MergeException
     */
    protected function mergeAllFiles($firstFilePath, &$otherFilesContent)
    {
        $this->zip->Open($firstFilePath);
        $finalContent = $this->zip->FileRead('word/document.xml');
        $p = strpos($finalContent, '</w:body>');

        if ($p === false) {
            throw new MergeException(MergeException::INVALID_DOCUMENT);
        }

        $finalContent = substr_replace($finalContent, $otherFilesContent, $p, 0);
        $this->zip->FileReplace('word/document.xml', $finalContent, TBSZIP_STRING);
    }

    /**
     * @param string $mergedFilePath
     * @return bool
     */
    protected function saveMergedFile($mergedFilePath)
    {
        return $this->zip->Flush(TBSZIP_FILE, $mergedFilePath);
    }
}